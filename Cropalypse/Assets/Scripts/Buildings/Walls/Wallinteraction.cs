using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wallinteraction : MonoBehaviour
{
    public float thrust = 12f;
    private Rigidbody2D rb2d;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Alien"))
        {
            rb2d.AddForce(transform.up * thrust, ForceMode2D.Impulse);
            Debug.Log("Enemy in trigger");
        }
    }
}
