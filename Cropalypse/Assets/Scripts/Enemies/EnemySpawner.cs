using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public AlienMovement[] AllAliens; 
    public Transform AlienSpawnpointTransform;
    private float RandomMin = 0f;
    private float RandomMax = 0.5f;
    public float RandomTransformMax = -5f;
    public float RandomTransformMin = 5f;
    private float SpawnerTimer = 0f;
    public float XPositionSpawn;
    public float YPositionSpawn;

    public void Update()
    {
        SpawnerTimer -= Time.deltaTime;
        if (SpawnerTimer <= 0f)
        {
            int randomAlienIndex = Random.Range(0, AllAliens.Length);
            AlienMovement randomAlien = AllAliens[randomAlienIndex];
            float randomisationOfSpawn = Random.Range(RandomTransformMin, RandomTransformMax);
            Vector3 randomOffset = new Vector3(0, randomisationOfSpawn, 0);
            AlienSpawnpointTransform.position += randomOffset;
            SpawnTimer();
            InstantiateEnemy(randomAlien); 
        }
    }

    public void SpawnTimer()
    {
        SpawnerTimer = Random.Range(RandomMin, RandomMax); 
    }

    public void InstantiateEnemy(AlienMovement alien)
    {
        if (AllAliens != null && AllAliens.Length > 0) 
        {
            GameObject instantiatedAlien = Instantiate(alien.AlienPrefab, AlienSpawnpointTransform.position, Quaternion.identity);
            AlienSpawnpointTransform.position = new Vector2(XPositionSpawn, YPositionSpawn);
            Debug.Log("Alien Spawned");
        }
        else
        {
            Debug.LogWarning("The Array is Empty");
        }
    }
}
