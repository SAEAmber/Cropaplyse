using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienMovement : MonoBehaviour
{
    public float movementSpeed = 5f;
    private Transform CenterOfMap;
    public float DistanceTrigger = 20f;
    private Rigidbody2D rb2D;
    public GameObject AlienPrefab;
    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        CenterOfMap = GameObject.FindGameObjectWithTag("Center")?.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (CenterOfMap == null)
        {
            return;
        }
        if (Vector2.Distance(transform.position, CenterOfMap.position) < DistanceTrigger)
        {
            Vector2 directionToCenter = (CenterOfMap.position - transform.position).normalized;
            transform.Translate(directionToCenter * movementSpeed * Time.deltaTime);
            Debug.Log("Moving to center");
        }
        else
        {
            Debug.LogWarning("The center is not detected enemy will not move fix detection distance");
        }
    }
}
